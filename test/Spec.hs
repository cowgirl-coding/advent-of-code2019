import Test.Hspec

import qualified CrossedWires

data Test a b = Test { input :: a, output :: b }

d1 = Test 
    { input = 
        [ "R75,D30,R83,U83,L12,D49,R71,U7,L72"
        , "U62,R66,U55,R34,D71,R55,D58,R83"
        ]
    , output = 159
    }
d2 = Test
    { input =
        [ "R98,U47,R26,D63,R33,U87,L62,D20,R33,U53,R51"
        , "U98,R91,D20,R16,D67,R40,U7,R15,U6,R7"
        ]
    , output = 135
    }
st1 = d1 { output = 610 }
st2 = d2 { output = 410 }

main :: IO ()
main = hspec $ do
    describe "Day 3: Crossed Wires" $ do
        it ("Returns " ++ (show $ output d1) ++ " for test input #1") $ do
            CrossedWires.getLeastDistance (input d1) `shouldBe` (output d1)
        it ("Returns " ++ (show $ output d2) ++ " for test input #2") $ do
            CrossedWires.getLeastDistance (input d2) `shouldBe` (output d2)
        it ("Returns " ++ (show $ output st1) ++ " for test input #3") $ do
            CrossedWires.getLeastPathSteps (input st1) `shouldBe` (output st1)
        it ("Returns " ++ (show $ output st2) ++ " for test input #4") $ do
            CrossedWires.getLeastPathSteps (input st2) `shouldBe` (output st2)
