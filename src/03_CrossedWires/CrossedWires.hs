module CrossedWires 
    ( getLeastDistance
    , getLeastPathSteps
    ) where

-- Day 3: Crossed Wires --

-- What we know about our paths:
-- 1. We're only looking at intersections between two different paths
-- 2. Intersections only occur between vertical lines from one path and
--    horizontal lines from the other.
-- 3. Vertical segments have a constant x value.
-- 4. Horizontal segments have a constant y value
-- 5. If two lines intersect, the only possible point of intersection is the
--    constant x value of the vertical line, and the constant y value of the
--    horizontal line
-- 6. For a horizontal line to intersect a vertical line, it's start point has
-- to be less that the constant x of the vertical line, and it's end point has
-- to be greater, and also it's constant y value has to be greater than the
-- start of the the vertical line, and less that the end of the vertical line.
-- If all that is true, the line does intersect, and the point of intersection
-- it at the only possible point of intersection.
--
-- With those premises in mind, our approach to finding intersections will be
-- to convert each given path to a list of line segements, sorted by
-- orientation and start position. We'll separate the vertical from horizontal
-- segments from each path and match them with the horizontal components from
-- the other path. Then we'll use each vertical from each path to do a
-- "vertical sweep" across every horizontal on the other path, testing if the
-- above conditions for an intersection are all true. For every horizontal that
-- meets every condition with the sweeping vertical, we'll have an intersection
-- at the vertical's constant x and the horizontal's constant y.

import           Data.List       (groupBy, sort, sortBy)
import           Data.List.Split (splitOn)

newtype Vertical   = Vertical   Segment deriving (Ord, Eq)
newtype Horizontal = Horizontal Segment deriving (Ord, Eq)

data Direction = R | U | L | D deriving Read
data Segment = Segment
    { start        :: Int
    , end          :: Int
    , constant     :: Int
    , terminus     :: Int
    , pathDistance :: Int
    } deriving (Ord, Eq)

type Instruction = (Direction, Distance)
type Point = (Int, Int)
type Path = [Instruction]
type Distance = Int
type SegmentAccumulator = (([Vertical],[Horizontal]),Point,Distance)

parsePath :: String -> Path
parsePath pathString =
    let elements = splitOn "," pathString
        parseInstruction (x:xs) = (read [x], read xs)
    in map parseInstruction elements

-- Convert a Path into a list of Segments. Note that horizontals are sorted by
-- start point, while verticals are sorted by their constant x value.
createPathSegments :: Path -> ([Vertical],[Horizontal])
createPathSegments path = 
    let initAccumulator = (([],[]),(0,0),0) :: SegmentAccumulator
        ((vs,hs),_,_) = foldl addSegment initAccumulator path
        reverseConstant = 
            (\(Vertical v1) (Vertical v2) -> 
                constant v2 `compare` (constant v1))
     in (sortBy reverseConstant vs, sort hs)

addSegment :: SegmentAccumulator -> Instruction -> SegmentAccumulator
addSegment ((vs,hs),origin,pathDistance) (dir,dist) = 
    let (p1,p2) = origin
        nextPathDistance = pathDistance + dist
    in
    case dir of
        R ->
            let nextX = p1 + dist
                nextOrigin = (nextX,p2)
                segment = Horizontal Segment
                    { start = p1
                    , end = nextX
                    , constant = p2
                    , terminus = nextX
                    , pathDistance = nextPathDistance
                    }
            in ((vs,segment:hs),nextOrigin,nextPathDistance)
        U ->
            let nextY = p2 + dist
                nextOrigin = (p1,nextY)
                segment = Vertical Segment
                    { start = p2
                    , end = nextY
                    , constant = p1
                    , terminus = nextY
                    , pathDistance = nextPathDistance
                    }
            in ((segment:vs,hs),nextOrigin,nextPathDistance)
        L ->
            let nextX = p1 - dist
                nextOrigin = (nextX,p2)
                segment = Horizontal Segment
                    { start = nextX 
                    , end = p1
                    , constant = p2
                    , terminus = nextX
                    , pathDistance = nextPathDistance
                    }
            in ((vs,segment:hs),nextOrigin,nextPathDistance)
        D ->
            let nextY = p2 - dist
                nextOrigin = (p1,nextY)
                segment = Vertical Segment
                    { start = nextY
                    , end = p2
                    , constant = p1
                    , terminus = nextY
                    , pathDistance = nextPathDistance
                    }
            in ((segment:vs,hs),nextOrigin,nextPathDistance)

-- Use the vertical segments from each path to sweep the horizontal segments of
-- the other path
findPathIntersections :: Path -> Path -> [(Vertical, Horizontal)]
findPathIntersections p1 p2 =
    let [(vs1,hs1),(vs2,hs2)] = 
            map createPathSegments [p1,p2]
        initAccumulator = [] :: [(Vertical, Horizontal)]
    in  concat $ zipWith (findIntersections initAccumulator) [vs1,vs2] [hs2,hs1]

-- For a list of vertical segments and a list of horizontal segments, sweep each
-- vertical through the horizontals and return intersection points.
--
-- We're defining a custom recursive function instead of using a simple fold to
-- support the following optimizations:
-- 1. Once a horizontal segment has been eliminated by start value, it won't be
-- compared to other verticals, which will have progressively lower start 
-- values.
-- 2. If we eliminate all the horizontals, we will return the accumulator
-- without continuing to iterating through the verticals.
findIntersections :: 
    [(Vertical, Horizontal)] ->
    [Vertical] -> 
    [Horizontal] -> 
    [(Vertical, Horizontal)]
findIntersections acc [] _ = acc
findIntersections acc _ [] = acc
findIntersections acc (v:vs) hs = 
    let (Vertical v') = v
        nextHs = takeWhile (\(Horizontal h) -> start h <= (constant v')) hs
        intersectingHs = sweepVertical v nextHs :: [Horizontal]
        formattedMatches = 
            zip (repeat v) intersectingHs :: [(Vertical,Horizontal)]
        nextAccumulator = formattedMatches ++ acc :: [(Vertical,Horizontal)]
    in findIntersections nextAccumulator vs nextHs

-- Find all intersections between a single vertical segment and a list of
-- horizontal segments. This is where we engage our understanding of how to find
-- intersections. If any horizontal passes every filter, it intersects the
-- given vertical at the constant x of the vertical and constant y of the
-- horizontal. We already know that horizontals do not start after constant v.
sweepVertical :: Vertical -> [Horizontal] -> [Horizontal]
sweepVertical verticalSegment hs =
    let (Vertical v) = verticalSegment
        intersectors = 
            filter   (\(Horizontal h) -> end v   > (constant h))
            $ filter (\(Horizontal h) -> start v < (constant h))
            $ filter (\(Horizontal h) -> end h   > (constant v)) hs
    in intersectors

distanceFromOrigin (Vertical v, Horizontal h) =
    (abs $ constant v) + (abs $ constant h)

-- A Segment's path distance is the number of path steps to its terminus. Since
-- the intersection will be before its terminus, we'll calulate how many path
-- our path distance overshoots by and subtract that from the path distance to
-- find the total path steps to that segment
addPathSteps :: (Vertical,Horizontal) -> Int 
addPathSteps (Vertical v,Horizontal h) =
    let pathSteps1 = (pathDistance v) - (abs $ (terminus v) - (constant h))
        pathSteps2 = (pathDistance h) - (abs $ (terminus h) - (constant v))
     in pathSteps1 + pathSteps2

pathToIntersections :: [String] -> [(Vertical, Horizontal)]
pathToIntersections pathStrings =
    let [path1,path2] = map parsePath $ pathStrings
    in findPathIntersections path1 path2

getLeastDistance :: [String] -> Int
getLeastDistance =
    minimum . (map distanceFromOrigin) . pathToIntersections

getLeastPathSteps :: [String] -> Int
getLeastPathSteps =
    minimum . (map addPathSteps) . pathToIntersections

main :: IO ()
main = do
    input <- lines <$> readFile "./03_input.txt"
    let leastDistance = getLeastDistance input
        leastPathSteps = getLeastPathSteps input
    putStrLn $ "Least Distance: " ++ (show leastDistance)    -- Expect 1084
    putStrLn $ "Least Path Steps: " ++ (show leastPathSteps) -- Expect 9240
